# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later

version = "1.2.0"

ci_labels = {
        "ignore_count": "[ci:ignore-count]",
        "skip_build": "[ci:skip-build]: already built successfully in CI",
        "skip_vercheck": "[ci:skip-vercheck]",
}
