#!/usr/bin/env python3
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Internally used to add the MR-ID to a given commit message. """

import os
import sys

# Full label line (e.g. "[ci:skip-build]: already built successfully in CI")
label_line = os.getenv("MRHLPR_LABEL")

if not label_line:
    print("This script is meant to be called internally by mrhlpr.py.")
    exit(1)

# Check if the label is in already in the commit message
label = f"{label_line.split(']', 1)[0]}]"
label_found = False
line_number = 0
empty_lines = 0
for line in sys.stdin:
    line = line.rstrip()
    line_number += 1

    if label in line:
        label_found = True

    if line == "":
        empty_lines += 1

    print(line)

if label_found:
    exit(0)

if line_number == 1:
    print()

# Add empty line before starting trailer section of commit message
if line_number >= 3 and empty_lines < 2:
    print()

print(label_line)
